

/*Credits: https://codepen.io/FrontEndBoard/pen/wPjBGM*/
$.fn.isInViewport = function() {
    var elementTop = $(this).offset().top;
    var elementBottom = elementTop + $(this).outerHeight();

    var viewportTop = $(window).scrollTop();
    var viewportBottom = viewportTop + $(window).height();

    return elementBottom > viewportTop && elementTop < viewportBottom;
};

$.fn.menuOpen = function() {
    this.css("width","auto");
    this.css("background","rgb(23, 33, 43)");
    this.css("border", "1px solid rgba(17, 28, 39, 1)");    
    /*this.css("background-image", "url(../img/polygon_night.jpg)");
    this.css("background-size","cover");*/
}

$.fn.menuClose = function() {
    this.css("width","0");
    this.css("background","rgba(0,0,0,0)");
    this.css("border","none");
}

// should profit of addClass and not call css-func

$(document).ready( function() {
    $('.fa-bars').click(function(){
        $(".nav-menu").menuOpen();    
    });
    $('#close').click(function(){
        $(".nav-menu").menuClose();
    });
});

$(window).scroll(function() {
   if( $(".landing").isInViewport()) {
       $("header").css("background", "rgba(0,0,0,0)"); 
       $("header").css("border", "0px solid rgba(0,0,0)");
       $("#title").hide();
    }
    else {
        $("header").css("background","rgb(23, 33, 43)");
        $("header").css("border", "1px solid rgba(17, 28, 39, 1)");
        $("#title").show();
    }
});